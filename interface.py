from machine import I2C, Pin, Timer
import ads1x15
from time import sleep_ms
from onewire import OneWire
from ds18x20 import DS18X20
import wdt
import properties
from umqtt.simple import MQTTClient
from ina219 import INA219


print("Init interface")

############ EMONCMS ############
mqttClient = MQTTClient("pvclient", "10.1.1.5", port=1883, user=properties.mqttUser, password=properties.mqttPassword)

############ Constants ############
HIGH=1
LOW=0

############ ADC Sensors ############
gain = 1
addr = 72
rate = 4
voltChannel1 = 0
ampereChannel1 = 1
count2mv=2
v2mv=1000
overflowLimit=100 #prevent < 0 reading looping around to max 

############ Common ############
irq_busy = False
SAMPLE_RATE_MS = 30000
sampleCount = 0
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)

############ Battery i2c sensor ############
SHUNT_OHMS = 0.01
ina = INA219(SHUNT_OHMS, i2c)
ina.configure()


############## GPIO ############
#Set up for the gravity tank float sensor
floatSensorPin = Pin(14, Pin.IN, Pin.PULL_UP)
#Used to avoid trying to pump at night
minPumpVoltageThreshold=15.0
#Temperatue threshold for swithcing on the water cooling DegC
temperatureCoolingThreshold=35.0
#Set up the water cooling SSR control 
waterCoolSSRControlPin=Pin(12, Pin.OUT, value=LOW) 
#Set up the Pump SSR control
pumpSSRControlPin=Pin(13, Pin.OUT, value=LOW) 

############## Display ############
ads = ads1x15.ADS1115(i2c, addr, gain)

############## Temperature Sensors ############
# create the onewire object
ds = DS18X20(OneWire(Pin(0)))
# scan for devices on the bus
roms = ds.scan()

def trySample(x):
	global irq_busy
	try:
		sample()
	except Exception as e:
		irq_busy=False
		print(e)

# Interrupt service routine for data acquisition
# called by a timer interrupt
def sample():
    print("Starting sample")
    global ads, irq_busy, sampleCount
    if irq_busy:
        return
    irq_busy = True

    volts = ads.read(rate, voltChannel1)
    volts = (volts*count2mv)/v2mv
    if volts>overflowLimit:
    	volts=0
    print("PV Volts: {:.1f} V".format(volts))

    amps = ads.read(rate, ampereChannel1)
    amps = (amps*count2mv)/v2mv
    if amps>overflowLimit:
    	amps=0
    print("PV Amps: {:.1f} A".format(amps))

    watts = volts * amps
    print("PV Watts: {:.1f} W".format(volts))


    battVolts = ina.voltage()
    print("Batt Volts: {:.1f} V".format(battVolts))

    battAmps = ina.current()/1000
    print("Batt Amps: {:.1f} A".format(battAmps))

    battWatts = ina.power()/1000
    print("Batt Watts: {:.1f} W".format(battWatts))

    ds.convert_temp()
    sleep_ms(750)

    pvTemp = ds.read_temp(roms[0])
    print("PV Temp: {:.1f} DegC".format(pvTemp))

    ambientTemp = ds.read_temp(roms[1])
    print("Ambient Temp: {:.1f} DegC".format(ambientTemp))

    hwcTemp = ds.read_temp(roms[2])
    print("HWC Temp: {:.1f} DegC".format(hwcTemp))

    #Check to see of gravity tank needs filling
    pumpOn=LOW
    if (floatSensorPin.value()==LOW) and (volts>minPumpVoltageThreshold):
		pumpSSRControlPin.value(HIGH)
		pumpOn=HIGH
    else:
        pumpSSRControlPin.value(LOW)
    print("Pump: {:0d}".format(pumpOn))

	#Check panels are too hot
    coolingOn=LOW
    if (pvTemp>temperatureCoolingThreshold):
        waterCoolSSRControlPin.value(HIGH)
        coolingOn=HIGH
    else:
        waterCoolSSRControlPin.value(LOW)

    print("Cooling: {:0d}".format(coolingOn))

    mqttClient.connect()
    print("MQTT Connected...sending")

    mqttClient.publish(b"emon/pv/volts", b"{:.1f}".format(volts))
    mqttClient.publish(b"emon/pv/amps", b"{:.1f}".format(amps))
    mqttClient.publish(b"emon/pv/watts", b"{:.1f}".format(watts))

    mqttClient.publish(b"emon/pv/battVolts", b"{:.1f}".format(battVolts))
    mqttClient.publish(b"emon/pv/battAmps", b"{:.1f}".format(battAmps))
    mqttClient.publish(b"emon/pv/battWatts", b"{:.1f}".format(battWatts))

    mqttClient.publish(b"emon/pv/pvTemp", b"{:.1f}".format(pvTemp))
    mqttClient.publish(b"emon/pv/ambientTemp", b"{:.1f}".format(ambientTemp))
    mqttClient.publish(b"emon/pv/hwcTemp", b"{:.1f}".format(hwcTemp))

    mqttClient.publish(b"emon/pv/pumpOn", b"{:.0f}".format(pumpOn))
    mqttClient.publish(b"emon/pv/coolingOn", b"{:.0f}".format(coolingOn))

    print("MQTT Data sent...disconecting")
    mqttClient.disconnect()

    sampleCount=sampleCount+1
    uptimeMs = (sampleCount*SAMPLE_RATE_MS)
    wdt.feed()
    irq_busy = False


def msToTime(ms):
    seconds=(ms/1000)%60
    seconds = int(seconds)
    minutes=(ms/(1000*60))%60
    minutes = int(minutes)
    hours=(ms/(1000*60*60))%24
    return ("%d:%d:%d" % (hours, minutes, seconds))

wdt.feed()
tim = Timer(-1)
tim.init(period=SAMPLE_RATE_MS, mode=Timer.PERIODIC, callback=trySample)

