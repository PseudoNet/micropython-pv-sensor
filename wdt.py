import time
import machine
#Custom software watchdog timer 

counter=0 # Cyciling counter for WDT
trigger=3 # Number of ticks before reset (105 seconds)
timer=machine.Timer(-1) # Container variable for timer object
timerIntervalms=35000 #tick interval (35 Seconds)

def tick(x):
    global counter
    counter+=1
    print("WDT Tick %s"%str(counter))
    if counter>=(trigger):
        print("WDT Reset")
        kill()
        time.sleep(3) # So you have time display reset message
        machine.reset()

def feed():
    print("WDT Feed")
    kill()
    timer.init(period=timerIntervalms,  mode=machine.Timer.PERIODIC, callback=tick)

def kill():
    global counter
    counter=0
    timer.deinit()