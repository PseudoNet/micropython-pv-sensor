# micropython-pv-sensor

Small collection of scripts that take data from various sensors and send it to a ceneral Emonpi via MQTT protocol.
Hardware components:
 * esp8266 NodeMCU V3 Dev board
 * 3x DS18X20 temperature sensors
 * ADS1115
 * INA-169 Voltage/Current sensor


![alt text](PVSensor.png)
